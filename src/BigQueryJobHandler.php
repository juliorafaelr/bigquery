<?php namespace juliorafaelr\BigQuery;

use Google\Cloud\BigQuery\Job;
use Google\Cloud\Core\ExponentialBackoff;

class BigQueryJobHandler {
	/**
	 * process a BigQuery Job, returns an array with the job information
	 *
	 * @param Job $job
	 * @param bool $returnResults
	 *
	 * @return array
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 */

	public static function processJob( Job $job, bool $returnResults = false ): array {
		$jobArray = array( );

		try {
			$jobArray[ $job->id( ) ][ 'status' ] = true;
			$jobArray[ $job->id( ) ][ 'results' ] = array( );


			// poll the job until it is complete
			$backOff = new ExponentialBackoff( 25 );

			$backOff->execute(function ( ) use ( $job ) {
				print( date( "Y-m-d H:i:s" ) . ' -> Waiting for job to complete' . PHP_EOL );

				$job->reload( );

				if ( $job->isComplete( ) === false ) {
					throw new \Exception( 'Job has not yet completed', 500 );
				}
			});

			if ( $returnResults === true ) {
				$result = array( );

				$queryResults = $job->queryResults( );

				if ( $queryResults->isComplete( ) === true ) {
					$rows = $queryResults->rows( );

					foreach ( $rows as $rowKey => $row ) {
						foreach ( $row as $column => $value ) {
							$result[ $rowKey ][ $column ] = $value;
						}
					}

					$jobArray[ $job->id( ) ][ 'results' ] = $result;

					$jobArray[ $job->id( ) ][ 'status' ] = true;
				}
				else {
					$jobArray[ $job->id( ) ][ 'error_message' ] = 'The query failed to complete';

					$jobArray[ $job->id( ) ][ 'status' ] = false;
				}
			}
			else {
				$error = '';

				// check if the job has errors
				if ( empty( $job->info( )[ 'status' ][ 'errors' ] ) === false ) {
					foreach ( $job->info( )[ 'status' ][ 'errors' ] as $errorMessage ) {
						$error .= $errorMessage[ 'message' ] . ' ';
					}
				}
				else if ( empty( $job->info( )[ 'status' ][ 'errorResult' ] ) === false ) {
					$error = $job->info( )[ 'status' ][ 'errorResult' ][ 'message' ];
				}

				if ( empty( $error ) === false ) {
					$jobArray[ $job->id( ) ][ 'error_message' ] = 'Error running BigQuery job: ' . trim( $error );

					$jobArray[ $job->id( ) ][ 'status' ] = false;
				}
				else {
					$jobArray[ $job->id( ) ][ 'status' ] = true;
				}
			}

		}
		catch ( \Google\Cloud\Core\Exception\ServiceException  $e ) {
			if ( stripos( $e->getMessage( ), "Retrying may solve the problem." ) !== false || stripos( $e->getMessage( ), "Service Unavailable" ) !== false || stripos( $e->getMessage( ), "An internal error occurred and the request could not be completed." ) ) {
				echo 'retrying' . PHP_EOL;

				sleep( 10 );

				return self::processJob( $job, $returnResults );
			}
			else {
				$jobArray[ $job->id( ) ][ 'error_message' ] = $e->getMessage( );

				$jobArray[ $job->id( ) ][ 'status' ] = false;
			}
		}
		catch ( \Exception  $e ) {
			if ( stripos( $e->getMessage( ), "Retrying may solve the problem." ) !== false || stripos( $e->getMessage( ), "Service Unavailable" ) !== false || stripos( $e->getMessage( ), "An internal error occurred and the request could not be completed." ) ) {
				echo 'retrying' . PHP_EOL;

				sleep( 10 );

				return self::processJob( $job, $returnResults );
			}
			else {
				$jobArray[ $job->id( ) ][ 'error_message' ] = $e->getMessage( );

				$jobArray[ $job->id( ) ][ 'status' ] = false;
			}
		}

		return $jobArray[ $job->id( ) ];
	}

	/**
	 * process Multiple BigQuery Job, returns an multidimensional array of processed jobs
	 *
	 * @param array $jobs
	 *
	 * @return array
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 */

	public static function processMultipleJobs( array $jobs ): array {
		$processedJob = array( );

		foreach ( $jobs as $job ) {
			$processedJob[ ] = self::processJob( $job[ 'object' ], $job[ 'return_results' ] );
		}

		return $processedJob;
	}
}