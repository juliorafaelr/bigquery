<?php namespace juliorafaelr\BigQuery;

use Google\Cloud\BigQuery\BigQueryClient;
use Google\Cloud\BigQuery\Dataset;
use Google\Cloud\BigQuery\QueryJobConfiguration;
use Google\Cloud\BigQuery\Table;
use Google\Cloud\Core\ExponentialBackoff;
use Google\Cloud\BigQuery\Job;
use juliorafaelr\ArrayHelper\ArrayHelper;
use juliorafaelr\DateHelper\DateHelper;
use juliorafaelr\GoogleStorage\GoogleStorage;

/**
 * class to handle principal actions of BigQuery API
 */
class BigQuery {
	/**
	 * @var BigQueryClient
	 */

	protected $bigQueryClient;

	/**
	 * @var string
	 */

	protected $projectId;

	/**
	 * @var GoogleStorage
	 */

	protected $storage;

	protected $job_try = 0;

	const job_max_try = 5;

	/**
	 * @var string;
	 */

	protected $keyFilePath;

	/**
	 * BigQuery constructor.
	 *
	 * @param string $projectId
	 * @param string $keyFilePath
	 */

	function __construct( string $projectId, string $keyFilePath ) {
		$this->projectId = $projectId;

		$this->keyFilePath = $keyFilePath;

		$this->bigQueryClient = new BigQueryClient(array(
			'keyFilePath' => $this->keyFilePath,
		));
	}

	/**
	 * Run synchronous query
	 *
	 * @param	string	$query
	 * @param	bool	$useLegacySql
	 * @param	array	$parameters
	 *
	 * @return	array
	 *
	 * @throws	\Google\Cloud\Core\Exception\GoogleException
	 * @throws	\Google\Cloud\Core\Exception\ServiceException
	 * @throws	\Exception
	 */

	public function runQuery( string $query, bool $useLegacySql = true, array $parameters = array( ) ): array {
		$function = __FUNCTION__;

		$jobConfig = $this->bigQueryClient->query( $query )->useLegacySql( $useLegacySql );

		if ( empty( $parameters ) === false ) {
			$jobConfig->parameters( $parameters );
		}

		$job = $this->createQueryJob( $this->bigQueryClient, $jobConfig );

		return $this->processJob( $job, true, $function, $query, $useLegacySql, $parameters );
	}

	/**
	 * @param	array	$queries
	 * @param	int		$numberParallelRequest
	 *
	 * @return	array
	 *
	 * @throws	\Google\Cloud\Core\Exception\GoogleException
	 * @throws	\Exception
	 */

	public function runParallelQueries( array $queries, int $numberParallelRequest = 100 ): array {
		$processedJobs = array( );

		$maxParallelRequest = 100;

		if ( $numberParallelRequest > $maxParallelRequest ) {
			$numberParallelRequest = $maxParallelRequest;
		}

		$queryArray = ArrayHelper::array_partition( $queries, $numberParallelRequest );

		foreach ( $queryArray as $queries ) {
			foreach ( $queries as $query ) {
				$jobs = array( );

				if ( array_key_exists( 'parameters', $query ) === false ) {
					$query[ 'parameters' ] = array( );
				}

				if ( array_key_exists( 'isLegacySql', $query ) === false ) {
					$query[ 'isLegacySql' ] = false;
				}

				$jobConfig = $this->bigQueryClient->query( $query[ 'statement' ] )->useLegacySql( $query[ 'isLegacySql' ] );

				if ( empty( $query[ 'parameters' ] ) === false ) {
					$jobConfig->parameters( $query[ 'parameters' ] );
				}

				$jobs[ ] =[
					'object' => $this->createQueryJob( $this->bigQueryClient , $jobConfig ),
					'return_results' => true,
				];

				foreach ( BigQueryJobHandler::processMultipleJobs( $jobs ) as $job ) {
					$processedJobs[ ] = $job;
				}
			}
		}

		return $processedJobs;
	}

	/**
	 * list DataSets
	 *
	 * @return array
	 */

	public function listDatasets(): array {
		$result = array( );

		$bigQuery = $this->bigQueryClient;

		$datasets = $bigQuery->datasets( );

		foreach ( $datasets as $dataset ) {
			$result[ ] = $dataset->id( );
		}

		return $result;
	}

	/**
	 * list Dataset's Tables
	 *
	 * @param $destination
	 * @param string|null $keyword
	 *
	 * @return array
	 */

	public function listDatasetTables( array $destination, ?string $keyword = null ): array {
		$result = array( );

		$bigQuery = $this->bigQueryClient;

		$dataset = $bigQuery->dataset( $destination[ 'datasetId' ] );

		$tables = $dataset->tables( );

		foreach ( $tables as $table ) {
			if ( empty( $keyword ) === false ) {
				if ( stripos( $table->id( ), $keyword ) !== false ) {
					$result[ ] = $table->id( );
				}
			}
			else {
				$result[ ] = $table->id( );
			}
		}

		return $result;
	}

	/**
	 * import the content of a file into a BigQuery table (create table if need it)
	 *
	 * @param array $bigQueryTableData
	 * @param string $source
	 * @param array $schema
	 * @param bool $truncateTable
	 * @param bool $skipLeadingRows
	 * @param string $fieldDelimiter
	 * @param string|null $partitionField
	 *
	 * @return bool
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 * @throws \Exception
	 */

	public function importFromFile( array $bigQueryTableData, string $source, array $schema = array( ), bool $truncateTable = true, bool $skipLeadingRows = false, string $fieldDelimiter = null, ?string $partitionField = null ): bool {
		$function = __FUNCTION__;

		$bigQuery = $this->bigQueryClient;

		$dataset = $bigQuery->dataset( $bigQueryTableData[ 'datasetId' ] );

		$table = $dataset->table( $bigQueryTableData[ 'tableId' ] );

		// create the import job
		$loadConfig = $table->load( fopen( $source,  'r' ) );

		$pathInfo = pathinfo( $source ) + [ 'extension' => null ];

		if ( 'csv' === $pathInfo[ 'extension' ] ) {
			$loadConfig->sourceFormat('CSV');

			$loadConfig->skipLeadingRows( ( !empty( $skipLeadingRows ) ) ? 1 : 0 );
		}
		else if ( 'json' === $pathInfo[ 'extension' ] ) {
			$loadConfig->sourceFormat('NEWLINE_DELIMITED_JSON');
		}
		else {
			throw new \InvalidArgumentException('Source format unknown. Must be JSON or CSV');
		}

		if ( empty( $schema ) === false ) {
			$loadConfig->schema( [ 'fields' => $schema ] );
		}
		else {
			$loadConfig->autodetect( true );
		}

		if ( empty( $fieldDelimiter ) === false ) {
			$loadConfig->fieldDelimiter( $fieldDelimiter );
		}

		$writeDisposition = $truncateTable ? 'WRITE_TRUNCATE' : 'WRITE_APPEND';

		$loadConfig->writeDisposition( $writeDisposition );

		$loadConfig->createDisposition( 'CREATE_IF_NEEDED' );

		$loadConfig->allowQuotedNewlines( true );

		if ( preg_match( '/\$([0-9]{2}\d{2})(\d{2})(\d{2})$/i', $bigQueryTableData[ 'tableId' ] ) ) {
			$realName = preg_replace( '/\$([0-9]{2}\d{2})(\d{2})(\d{2})$/i', "", $bigQueryTableData[ 'tableId' ] );

			$newTableObj = [
				'projectId' => $bigQueryTableData[ 'projectId' ],
				'datasetId' => $bigQueryTableData[ 'datasetId' ],
				'tableId' => $realName
			];

			if ( !$this->ifTableExist( $newTableObj ) ) {
				if ( !empty( $schema ) ) {
					$partitionConfig[ 'type' ] = 'DAY';

					if ( empty( $partitionField ) === false ) {
						$partitionConfig[ 'field' ] = $partitionField;
					}

					$this->createTable( $newTableObj, $schema, $partitionConfig );
				}
				else{
					throw new \Exception( 'you are trying to create an nonexistent partitioned table. you need to specify a schema' );
				}
			}
		}

		// instantiate the bigquery table service
		$job = $table->runJob( $loadConfig );

		return $this->processJob( $job, false, $function, $bigQueryTableData, $source, $schema, $truncateTable, $skipLeadingRows, $fieldDelimiter, $partitionField );
	}

	/**
	 * Create Dataset
	 *
	 * @param string $datasetId
	 *
	 * @return Dataset
	 */

	public function createDataset( string $datasetId ): Dataset {
		$bigQuery = $this->bigQueryClient;

		$dataset = $bigQuery->dataset( $datasetId );

		if ( $dataset->exists( ) === false ) {
			return $bigQuery->createDataset( $datasetId );
		}
		else {
			return $dataset;
		}
	}

	/**
	 * Delete a BigQuery the table.
	 *
	 * @param array $bigQueryTableData
	 */

	public function deleteTable( array $bigQueryTableData ): void {
		if ( $this->ifTableExist( $bigQueryTableData ) === true ) {
			$bigQuery = $this->bigQueryClient;

			$dataset = $bigQuery->dataset( $bigQueryTableData[ 'datasetId' ] );

			$table = $dataset->table( $bigQueryTableData[ 'tableId' ] );

			$table->delete( );
		}
	}

	/**
	 * creates a bigQuery table.
	 *
	 * Example:
	 * ```
	 * $bigQuery = new BigQuery($projectId);
	 * $bigQuery->createTable($bigQueryTableData, $schema, ['type'=> 'DAY', expirationMs = 3600])
	 * ```
	 *
	 * @param array $bigQueryTableData [
	 *      'projectId' => 'xxx',
	 *      'datasetId' => 'profileIdTest',
	 *      'tableId'   => 'partitiontest$20170531'
	 *  ];
	 * @param array $schema $schema[0] = [
	 *      'name' => 'field_test',
	 *      'type' => 'STRING',
	 *      'mode' => 'NULLABLE'
	 * ]
	 * @param array $timePartitioningOptions ['type'=> 'DAY', expirationMs = 3600]
	 *
	 * @see https://cloud.google.com/bigquery/docs/reference/rest/v2/tables API Table Object Representation.
	 *
	 * @return \Google\Cloud\BigQuery\Table
	 *
	 * @throws \Exception
	 */

	public function createTable( array $bigQueryTableData, array $schema, array $timePartitioningOptions = array( ) ) {
		$bigQuery = $this->bigQueryClient;

		$dataset = $bigQuery->dataset( $bigQueryTableData[ 'datasetId' ] );

		$options[ 'schema' ][ 'fields' ] = $schema;

		if ( empty( $timePartitioningOptions ) === false ) {
			$options[ 'timePartitioning' ] = $timePartitioningOptions;
		}

		return $dataset->createTable( $bigQueryTableData[ 'tableId' ], $options );
	}

	/**
	 * update Table Metadata
	 *
	 * @param array $bigQueryTableData
	 * @param array $schema
	 * @param array $options
	 *
	 * @return Table
	 */

	public function updateTable( array $bigQueryTableData, array $schema, array $options = array( ) ): Table {
		$bigQuery = $this->bigQueryClient;

		$dataset = $bigQuery->dataset( $bigQueryTableData[ 'datasetId' ] );

		$metadata[ 'schema' ][ 'fields' ] = $schema;

		$table = $dataset->table( $bigQueryTableData[ 'tableId' ] );

		$table->update( $metadata, $options );

		return $table;
	}

	/**
	 * Run a load job which loads data from a file in a Storage bucket into the table.
	 *
	 * IMPORTANT : SEE OPTIONS IF NEEDED: https://cloud.google.com/bigquery/docs/loading-data-cloud-storage-csv
	 *
	 * Example:
	 * ```
	 * $object = $storage->bucket('myBucket')->object('important-data.csv');
	 * $job = $table->load($object);
	 * ```
	 *
	 * @see https://cloud.google.com/bigquery/docs/reference/v2/jobs Jobs insert API Documentation.
	 *
	 * @param array $destinationTable
	 * @param array|null $bucketData
	 * @param array $job_options
	 * @param string|null $partitionField
	 *
	 * @return bool
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 * @throws \Exception
	 */

	public function loadFromStorage( array $destinationTable, ?array $bucketData, array $job_options = array( ), ?string $partitionField = null ): bool {
		try {
			$function = __FUNCTION__;

			$dataset = $this->bigQueryClient->dataset( $destinationTable[ 'datasetId' ] );

			$table = $dataset->table( $destinationTable[ 'tableId' ] );

			if ( empty( $bucketData ) === false ) {
				$storage = new GoogleStorage( $this->keyFilePath );

				list( $object, $info ) = $storage->getStorageObjectObject( $bucketData );

				if ( (int)$info[ 'size' ] === 0 ) {
					throw new \Exception ( 'file is empty. nothing to load' );
				}

				$loadConfig = $table->loadFromStorage( $object );

				if ( '.backup_info' === substr( $bucketData[ 'objectName' ], -12 ) ) {
					$loadConfig->sourceFormat( 'DATASTORE_BACKUP' );
				}
				else if ( '.json' === substr( $bucketData[ 'objectName' ], -5 ) ) {
					$loadConfig->sourceFormat( 'NEWLINE_DELIMITED_JSON' );
				}
				else if ( '.avro' === substr( $bucketData[ 'objectName' ], -5 ) ) {
					$loadConfig->sourceFormat( 'AVRO' );
				}
				else if ( '.csv' === substr( $bucketData[ 'objectName' ], -4 ) ) {
					$loadConfig->sourceFormat( 'CSV' );
				}
			}
			else {
				$loadConfig = $table->load( null );
			}

			if ( preg_match( '/\$([0-9]{2}\d{2})(\d{2})(\d{2})$/i', $destinationTable[ 'tableId' ] ) ) {
				$partitionConfig[ 'type' ] = 'DAY';

				if ( empty( $partitionField ) === false ) {
					$partitionConfig[ 'field' ] = $partitionField;
				}

				$loadConfig->timePartitioning( $partitionConfig );
			}

			// determine the import options from the object name
			if ( empty( $job_options ) === false ) {
				foreach ( $job_options as $optionName => $value ) {
					$loadConfig->$optionName( $value );
				}
			}

			$loadConfig->createDisposition( 'CREATE_IF_NEEDED' );

			$job = $table->runJob( $loadConfig );

			return $this->processJob( $job, false, $function, $destinationTable, $bucketData, $job_options, $partitionField );
		}
		catch ( \Google\Cloud\Core\Exception\ServiceException $e ) {
			if ( stripos( $e->getMessage( ), 'Not found: URI' ) === false ) {
				throw new \Google\Cloud\Core\Exception\ServiceException( $e->getMessage( ) );
			}

			return false;
		}
		catch ( \Google\Cloud\Core\Exception\GoogleException $e ) {
			if ( stripos( $e->getMessage( ), 'Not found: URI' ) === false ) {
				throw new \Google\Cloud\Core\Exception\GoogleException( $e->getMessage( ) );
			}

			return false;
		}
		catch ( \Exception $e ) {
			if ( stripos( $e->getMessage( ), 'Not found: URI' ) === false ) {
				throw new \Exception( $e->getMessage( ) );
			}

			return false;
		}
	}

	/**
	 * creates (if not exist) and writes into a bigQuery table from a BigQuery Statement on other Table.
	 *
	 * Example:
	 * ```
	 * $bigQuery = new BigQuery( $projectId, $keyFile );
	 * $bigQuery->importFromQuery("SELECT * FROM [] LIMIT 10;", $destination,true,true);
	 * ```
	 *
	 * @param string $query BigQuery Statement.
	 * @param array $destination [
	 *      'projectId' => 'xxx',
	 *      'datasetId' => 'profileIdTest',
	 *      'tableId'   => 'partitiontest$20170531'
	 *  ];
	 * @param boolean $useLegacySql defines whether to use legacy statements or not.
	 * @param boolean $createPartitionedTable defines whether to create partitioned table or not.
	 * @param bool $flattenResults
	 * @param bool $truncateDestinationTable
	 * @param array $timePartitioningOptions
	 *
	 * @return bool whether the process is successful or not.
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 * @throws \Exception
	 */

	public function importFromQuery( string $query, array $destination, bool $useLegacySql = true, bool $createPartitionedTable = false, bool $flattenResults = false, bool $truncateDestinationTable = true, array $timePartitioningOptions = array( ) ): bool {
		$function = __FUNCTION__;

		$writeDisposition = $truncateDestinationTable ? 'WRITE_TRUNCATE' : 'WRITE_APPEND';

		$BigQueryDestination = new BigQuery( $destination[ 'projectId' ], $this->keyFilePath );

		if ( $createPartitionedTable === true ) {
			$realName = preg_replace( '/\$([0-9]{2}\d{2})(\d{2})(\d{2})$/i', "", $destination[ 'tableId' ] );

			$newDestination = [
				'projectId' => $destination[ 'projectId' ],
				'datasetId' => $destination[ 'datasetId' ],
				'tableId'   => $realName
			];

			if ( $BigQueryDestination->ifTableExist( $newDestination ) === false ) {
				$BigQueryDestination->createPartitionedTableFromQuery( $query, $newDestination, $useLegacySql, $timePartitioningOptions );
			}
		}

		$jobConfig = $this->bigQueryClient->query( $query )->useLegacySql( $useLegacySql );

		$bigQuery2 = new BigQueryClient([
			'keyFilePath' => $this->keyFilePath,
			'scopes' => [
				'https://www.googleapis.com/auth/drive',
				'https://www.googleapis.com/auth/bigquery'
			],
			'projectId'   => $destination[ 'projectId' ]
		]);

		$dataset = $bigQuery2->dataset( $destination[ 'datasetId' ] );

		$table = $dataset->table( $destination[ 'tableId' ] );

		$jobConfig->allowLargeResults( true );
		$jobConfig->flattenResults( $flattenResults );
		$jobConfig->destinationTable( $table );
		$jobConfig->createDisposition( 'CREATE_IF_NEEDED' );
		$jobConfig->writeDisposition( $writeDisposition );
		$jobConfig->useQueryCache( false );

		$job = $this->createQueryJob( $this->bigQueryClient, $jobConfig );

		return $this->processJob( $job, false, $function, $query, $destination, $useLegacySql, $createPartitionedTable, $flattenResults, $truncateDestinationTable, $timePartitioningOptions );
	}

	/**
	 * Export a BigQuery table To Google Storage
	 *
	 * @param array $sourceData
	 * @param array $bucketDestinationData
	 * @param string $format
	 * @param bool $includeHeaders
	 *
	 * @return bool
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 */

	public function exportToGoogleStorage( array $sourceData, array $bucketDestinationData, string $format = 'csv', bool $includeHeaders = true ): bool {
		$function = __FUNCTION__;

		$bigQuery = new BigQueryClient([
			'keyFilePath' => $this->keyFilePath,
			'scopes' => [
				'https://www.googleapis.com/auth/drive',
				'https://www.googleapis.com/auth/bigquery'
			],
			'projectId'   => $sourceData[ 'projectId' ]
		]);

		$dataset = $bigQuery->dataset( $sourceData[ 'datasetId' ] );

		$table = $dataset->table( $sourceData[ 'tableId' ] );

		if ( (int)$table->info( )[ 'numRows' ] > 0 ) {
			// load the storage object
			$this->storage = new GoogleStorage( $this->keyFilePath );

			$storage = $this->storage->getStorageClientObject( );

			if ( $storage->bucket( $bucketDestinationData[ 'bucketName' ] )->exists( ) === false ) {
				$storage->createBucket( $bucketDestinationData[ 'bucketName' ] );
			}

			$destinationObject = $storage->bucket( $bucketDestinationData[ 'bucketName' ] )->object( $bucketDestinationData[ 'objectName' ] );

			// create the import job
			$loadConfig = $table->extract( $destinationObject );
			$loadConfig->destinationFormat( $format );
			$loadConfig->printHeader( $includeHeaders );

			$job = $table->runJob( $loadConfig );

			return $this->processJob( $job, false, $function, $sourceData, $bucketDestinationData, $format,$includeHeaders );
		}
		else {
			return true;
		}
	}

	/**
	 * copy table
	 *
	 * @param array $sourceData
	 * @param array $destinationData
	 * @param bool $truncateTable
	 *
	 * @return bool
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 */

	public function copyTable( array $sourceData, array $destinationData, bool $truncateTable = true ): bool {
		$function = __FUNCTION__;

		$bigQuery = $this->bigQueryClient;

		//Define Source Table Object
		$dataset = $bigQuery->dataset( $sourceData[ 'datasetId' ] );

		$table = $dataset->table( $sourceData[ 'tableId' ] );

		//Define Destination Table Object
		if ( $destinationData[ 'projectId' ] == $sourceData[ 'projectId' ] || empty( $destinationData[ 'projectId' ] ) === true ) {
			$destinationBigQuery = $bigQuery;
		}
		else {
			$destinationBigQuery = new BigQueryClient([
				'keyFilePath' => $this->keyFilePath,
				'scopes' => [
					'https://www.googleapis.com/auth/drive',
					'https://www.googleapis.com/auth/bigquery'
				],
				'projectId'   => $destinationData[ 'projectId' ]
			]);
		}

		$destinationDataSet = $destinationBigQuery->dataset( $destinationData[ 'datasetId' ] );

		$destinationTable   = $destinationDataSet->table( $destinationData[ 'tableId' ] );

		// create copy job
		$writeDisposition = $truncateTable ? 'WRITE_TRUNCATE' : 'WRITE_APPEND';

		$loadConfig = $table->copy( $destinationTable );

		$loadConfig->writeDisposition( $writeDisposition );

		$job = $table->runJob( $loadConfig );

		return $this->processJob( $job, false, $function, $sourceData, $destinationData, $truncateTable );
	}

	/**
	 * Check whether or not the table exists.
	 *
	 * @param array $destination
	 * @return bool
	 */

	public function ifTableExist( array $destination ): bool {
		$bigQuery = $this->bigQueryClient;

		$dataset = $bigQuery->dataset( $destination[ 'datasetId' ] );

		$table = $dataset->table( $destination[ 'tableId' ] );

		return $table->exists( );
	}

	/**
	 * get BigQuery Table Schema
	 *
	 * @param array $destination
	 *
	 * @return array
	 */

	public function getTableSchema( array $destination ): array {
		$bigQuery = $this->bigQueryClient;

		$dataset = $bigQuery->dataset( $destination[ 'datasetId' ] );

		$table = $dataset->table( $destination[ 'tableId' ] );

		$tableInfo = $table->info( );

		return $tableInfo[ 'schema' ][ 'fields' ];
	}

	/**
	 * Creates a partitioned table from a Query
	 *
	 * @param string $query
	 * @param array $destination
	 * @param bool $useLegacySql
	 * @param array $timePartitioningOptions
	 *
	 * @return \Google\Cloud\BigQuery\Table
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 * @throws \Exception
	 */

	private function createPartitionedTableFromQuery( string $query, array $destination, bool $useLegacySql, array $timePartitioningOptions ) {
		$schema = array( );

		$tempDestination = [
			'projectId' => $destination[ 'projectId' ],
			'datasetId' => $destination[ 'datasetId' ],
			'tableId'   => $destination[ 'tableId' ] . "_partitioned_temp_" . date( 'Ymdhms' ) . '_' . rand( 0, 1000000000 ),
		];

		$new_query = trim( preg_replace( '/(limit )(\d+)/i', "", str_replace( ";", "", $query ) ) ) . ' limit 1;';

		$tempTableIsCreated = $this->importFromQuery( $new_query, $tempDestination, $useLegacySql );

		if ( $tempTableIsCreated === true ) {
			$schema = $this->getTableSchema( $tempDestination );

			$this->deleteTable( $tempDestination );
		}

		if ( empty( $timePartitioningOptions ) === false ) {
			return $this->createTable( $destination, $schema, $timePartitioningOptions );
		}
		else {
			return $this->createTable( $destination, $schema, array( 'type' => 'DAY' ) );
		}
	}

	/**
	 * export a results of a BigQuery Query to a file into Google Storage.
	 *
	 * @param	string		$query
	 * @param	string|null	$local_file
	 * @param	array|null	$bucketDestinationData
	 * @param	bool		$useLegacySql
	 * @param	string		$format
	 * @param	bool		$flattenResults
	 * @param	bool		$includeHeaders
	 *
	 * @return	array		$downloadedLocalFiles	list of pathFiles downloaded on the FileSystem
	 *
	 * @throws	\Exception
	 */

	public function ExportFromQuery( string $query, ?string $local_file = null, ?array $bucketDestinationData = null, bool $useLegacySql = true, string $format = 'csv', bool $flattenResults = false, bool $includeHeaders = true ): array {
		$bqc = $this->getBigQueryClient( );

		if ( $bqc->dataset( 'user_extract' )->exists( ) === false ) {
			$bqc->createDataset( 'user_extract' );
		}

		$bigQueryDestination = [
			'projectId' => $this->projectId,
			'datasetId' => 'user_extract',
			'tableId'   => 'temp_table_' . date('YmdHis' ) . '_' . rand( 0, 1000000000 ),
		];

		if ( empty( $bucketDestinationData ) === true ) {
			$bucketDestinationData = [
				'projectId'  => $this->projectId,
				'bucketName' => 'mi-export',
				'objectName' => 'temp_file_' . date('YmdHis') . '_' . rand( 0, 1000000000 ) . '_*'
			];
		}

		if ( $format === 'csv' ) {
			$flattenResults = true;
		}

		$this->importFromQuery( $query, $bigQueryDestination, $useLegacySql, false, $flattenResults );

		$downloadedLocalFiles = $this->tableToStorage( $bigQueryDestination, $bucketDestinationData, $local_file, $format, $includeHeaders );

		$this->deleteTable( $bigQueryDestination );

		return $downloadedLocalFiles;
	}

	/**
	 * Download a files from the storage to a predefine path, and delete it (optional).
	 *
	 * @param	string		$bucketName			bucket on Storage
	 * @param	string		$objectName			object (including path) on Storage
	 * @param	string|null	$downloadFilePath	filesystem Path including name of the file for the download
	 * @param	bool 		$deleteObject		flag that indicates if the storage file must be deleted after download
	 *
	 * @return	string		$downloadFilePath	downloaded filesystem Path including name of the file
	 *
	 * @throws	\Exception
	 */

	private function downloadExportedFile( string $bucketName, string $objectName, ?string $downloadFilePath, bool $deleteObject = false ): string {
		$storage = $this->storage;

		$storage->downloadObject( $bucketName, $objectName, $downloadFilePath );

		if ( $deleteObject === true ) {
			$storage->deleteObject( $bucketName, $objectName );
		}

		return $downloadFilePath;
	}

	/**
	 * Export a BigQuery table to the storage, and download the files to a Local Folder if a download File Path is included (optional)
	 *
	 * @param    array $bigQueryTable
	 * @param    array $bucketDestinationData
	 * @param    string|null $downloadFilePath
	 * @param    string $format
	 * @param    bool $includeHeaders
	 *
	 * @return    array        $downloadedLocalFiles    list of pathFiles downloaded on the FileSystem
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 * @throws \Exception
	 */

	public function tableToStorage( array $bigQueryTable, array $bucketDestinationData, ?string $downloadFilePath, string $format = 'csv', bool $includeHeaders = true ): array {
		$downloadedLocalFiles = array( );

		$this->exportToGoogleStorage( $bigQueryTable, $bucketDestinationData, $format, $includeHeaders );

		if ( empty( $downloadFilePath ) === false ) {
			if ( stripos( $bucketDestinationData[ 'objectName' ], '*' ) !== false ) {
				$prefix = explode( '*', $bucketDestinationData[ 'objectName' ] )[ 0 ];

				$objects = $this->storage->listObjects( $bucketDestinationData[ 'bucketName' ], [ 'prefix' => $prefix ] );

				foreach ( $objects as $part => $objectName ) {
					$nameArray = explode( '.', $downloadFilePath );

					$extension = end( $nameArray );

					$fileName = str_replace( '.' . $extension, '', $downloadFilePath);

					$newDownloadFilePath = $fileName . '_part_'. $part .'.'. $extension;

					$downloadedLocalFiles[ ] = $this->downloadExportedFile( $bucketDestinationData[ 'bucketName' ], $objectName, $newDownloadFilePath, true );
				}
			}
			else {
				$downloadedLocalFiles[ ] = $this->downloadExportedFile( $bucketDestinationData[ 'bucketName' ], $bucketDestinationData[ 'objectName' ], $downloadFilePath, true );
			}
		}

		return $downloadedLocalFiles;
	}

	/**
	 *
	 * @param $origin
	 * @param $destination
	 * @param $date
	 * @param array $fields
	 * @param bool $useLegacySql
	 * @param bool $createPartitionedTable
	 * @param bool $flattenResults
	 * @param bool $truncateDestinationTable
	 *
	 * @return bool
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 */

	public function transferBetweenPartitionedTables( array $origin, array $destination, string $date, array $fields = array( ), bool $useLegacySql = true, bool $createPartitionedTable = false, bool $flattenResults = false, bool $truncateDestinationTable = true ): bool {
		$destination[ 'tableId' ] = $destination[ 'tableId' ] . '$' . date( 'Ymd', strtotime( $date ) );

		$query_fields = ( empty( $fields ) === true ) ? '*' : implode( ',', $fields );

		$query = /** @lang BigQuery */
			"SELECT $query_fields FROM `" . $origin['projectId'] . "`.`" . $origin['datasetId'].".".$origin [ 'tableId' ]. "` WHERE date = timestamp('".$date."')";

		return $this->importFromQuery( $query, $destination, $useLegacySql, $createPartitionedTable, $flattenResults, $truncateDestinationTable );
	}

	/**
	 * process a BigQuery Job, returns the response of the job if is a Query
	 *
	 * @param Job $job
	 * @param bool $returnResults
	 *
	 * @param string $function
	 * @param array $params
	 * @return array|bool
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 * @throws \Exception
	 */

	private function processJob( Job $job, bool $returnResults, string $function, ...$params ) {
		try {
			// poll the job until it is complete
			$backOff = new ExponentialBackoff( 25 );

			$backOff->execute( function( ) use ( $job ) {
				$job->reload( );

				if ( $job->isComplete( ) === false ) {
					throw new \Exception( 'Job has not yet completed', 500 );
				}
			});

			if ( $returnResults === true ) {
				$result = array( );

				$queryResults = $job->queryResults( );

				if ( $queryResults->isComplete( ) === true ) {
					$rows = $queryResults->rows( );

					foreach ( $rows as $rowKey => $row ) {
						foreach ( $row as $column => $value ) {
							$result[ $rowKey ][ $column ] = $value;
						}
					}

					$response = $result;
				}
				else {
					throw new \Exception( 'The query failed to complete' );
				}
			}
			else {
				$error = '';

				// check if the job has errors
				if ( empty( $job->info( )[ 'status' ][ 'errors' ] ) === false ) {
					foreach ( $job->info( )[ 'status' ][ 'errors' ] as $errorMessage ) {
						$error .= $errorMessage[ 'message' ] . ' ';

						if ( empty( $errorMessage[ 'location' ] ) === false ) {
							$error .= 'location: ' . $errorMessage[ 'location' ] . ' ';
						}
					}
				}
				else if ( empty( $job->info( )[ 'status' ][ 'errorResult' ] ) === false ) {
					$error = $job->info( )[ 'status' ][ 'errorResult' ][ 'message' ];

					if ( empty( $job->info( )[ 'status' ][ 'errorResult' ][ 'location' ] ) === false ) {
						$error .= ' location: ' . $job->info( )[ 'status' ][ 'errorResult' ][ 'location' ] . ' ';
					}
				}

				if ( empty( $error ) === false ) {
					throw new \Exception( 'Error running BigQuery job: ' . trim( $error ) );
				}
				else {
					$response = true;
				}
			}
		}
		catch ( \Google\Cloud\Core\Exception\ServiceException $e ) {
			try {
				throw new \Exception( $e->getMessage( ) );
			}
			catch( \Exception $e ) {
				return $this->onJobError( $e, $function, $params );
			}
		}
		catch ( \Exception  $e ) {
			return $this->onJobError( $e, $function, $params );
		}

		$this->job_try = 0;

		return $response;
	}

	/**
	 * create a BigQuery Query Job
	 *
	 * @param BigQueryClient $bigQueryClient
	 * @param QueryJobConfiguration $jobConfig
	 *
	 * @return Job
	 *
	 * @throws \Exception
	 */

	private function createQueryJob( BigQueryClient $bigQueryClient, QueryJobConfiguration $jobConfig ): Job {
		try {
			$job = $bigQueryClient->startQuery( $jobConfig );
		}
		catch ( \Google\Cloud\Core\Exception\ServiceException $e ) {
			try {
				throw new \Exception( $e->getMessage( ) );
			}
			catch( \Exception $e ) {
				$job = $this->onStartQueryError( $bigQueryClient, $jobConfig, $e );
			}
		}
		catch ( \Exception  $e ) {
			$job = $this->onStartQueryError( $bigQueryClient, $jobConfig, $e );
		}

		$this->job_try = 0;

		return $job;
	}

	/**
	 * what to do when a job fails.
	 *
	 * @param	\Exception	$e			'Exception' object
	 * @param	string 		$function
	 * @param 	$params
	 * @return	array|bool
	 *
	 * @throws	\Exception
	 */

	private function onJobError( \Exception $e, string $function, $params ): bool {
		$this->job_try++;

		if ( $this->job_try <= self::job_max_try ) {
			if ( stripos( $e->getMessage( ), 'Retrying may solve the problem.' ) !== false || stripos( $e->getMessage( ), 'Service Unavailable' ) !== false || stripos( $e->getMessage( ), 'An internal error occurred and the request could not be completed.' ) !== false ) {
				sleep( 10 );

				return call_user_func_array( array( $this, $function ), $params );
			}
			else if ( stripos( $e->getMessage( ), 'Your project exceeded quota for exports' ) !== false ) {
				sleep( 10800 );

				return call_user_func_array( array( $this, $function ), $params );
			}
			else {
				throw $e;
			}
		}
		else {
			throw $e;
		}
	}

	/**
	 * what to do when a QueryStart fails
	 *
	 * @param	BigQueryClient			$bigQueryClient
	 * @param	QueryJobConfiguration	$jobConfig
	 * @param	\Exception				$e
	 *
	 * @return	Job
	 *
	 * @throws	\Exception
	 */

	private function onStartQueryError( BigQueryClient $bigQueryClient, QueryJobConfiguration $jobConfig , \Exception $e ): Job {
		$this->job_try++;

		if ( stripos( $e->getMessage( ), 'Retrying may solve the problem.' ) !== false || stripos( $e->getMessage( ), 'Service Unavailable' ) !== false || stripos( $e->getMessage( ), 'An internal error occurred and the request could not be completed.' ) !== false ) {
			if ( $this->job_try <= self::job_max_try ) {
				echo json_encode( array(
					'status' => true,
					'event_tome' => time( ),
					'message' => 'retrying'
				) );

				sleep( 10 );

				return $this->createQueryJob( $bigQueryClient, $jobConfig );
			}
			else {
				throw $e;
			}
		}
		else {
			throw $e;
		}
	}

	/**
	 * build basic a BigQuery schema from array (repeated fields are not Supported, all fields are NULLABLE)
	 *
	 * @param	array	$fields
	 *
	 * @return	array
	 */

	public static function buildSchemaFromArray( array $fields ): array {
		$schemaString = '[';

		foreach ( $fields as $name => $type ) {
			$schemaString .= '{"name":"' . $name . '","type":"' . $type . '","mode":"NULLABLE"},';
		}

		$schemaString = substr( $schemaString, 0, -1 );

		$schemaString .= ']';

		return json_decode( $schemaString, true );
	}

	/**
	 * make storage files retro-compatibles by transforming it structure base on a new BigQuery Schema
	 *
	 * @param	array			$subject 					BigQuery table origin with some rules like new schema field, field ID, Field ID, Partition Field
	 * @param	array 			$BucketDestination			Bucket where the data will be placed
	 * @param	string 			$objectFileName				object file prefix (if is empty BigQuery table name will be taken)
	 * @param	string 			$objectPrefix				object prefix before sub-folder base on dates appears
	 * @param 	callable|null	$idTransformationCallback	a function that will transform id format for files name
	 * @param	array|null		$dates						dates that the retro compatibility will take place
	 * @param	string			$formatDateOnFileName		format date to be included on files name that will be put between table name and main id (if empty, it wont be added) date format php @see http://php.net/manual/en/function.date.php
	 *
	 * @throws	\Google\Cloud\Core\Exception\GoogleException
	 * @throws	\Google\Cloud\Core\Exception\ServiceException
	 * @throws	\Exception
	 *
	 * @return void
	 */

	public function storageRetroCompatibility( array $subject, array $BucketDestination, string $objectFileName = '', string $objectPrefix = '', ?callable $idTransformationCallback = null, ?array $dates = null, string $formatDateOnFileName = '' ): void {
		$__objectPrefix = '';

		if ( empty( $objectFileName ) === true ) {
			$objectFileName = $subject[ 'origin' ][ 'tableId' ];
		}

		if ( empty( $objectPrefix ) === false ) {
			$__objectPrefix = $objectPrefix . '/';
		}

		$fields = ( empty( $subject[ 'fields' ] ) === false ) ? implode( ',', $subject[ 'fields' ] ) : '*';

		if ( empty( $dates ) === true ) {
			list( $start_date, $end_date ) = $this->getTableDataLifeTime( $subject[ 'origin' ],  $subject[ 'partition_field' ] );

			$dates = DateHelper::getDatesByRange( $start_date, $end_date );
		}

		foreach ( $dates as $date ) {
			$dateFormat = '';

			$ids = $this->getActiveIdsForDate( $subject[ 'origin' ], $subject[ 'id' ], $subject[ 'partition_field' ], $date );

			echo json_encode( array(
					'status' => true,
					'event_time' => time( ),
					'message' => 'date ' . $date . ' table ' . $objectFileName,
				) ) . PHP_EOL;

			foreach ( $ids as $id ) {
				if ( $subject [ 'id_type' ] === 'STRING' ) {
					$singleIdQuery = $subject[ 'id' ] . ' = \'' . $id . '\'';
				}
				else {
					$singleIdQuery = $subject[ 'id' ] . ' = ' . $id ;
				}

				$query = /** @lang pg */
					'
					SELECT
						' . $fields . '
					FROM
						`' . $subject[ 'origin' ][ 'projectId' ] . '.' . $subject[ 'origin' ][ 'datasetId' ] . '.' . $subject[ 'origin' ][ 'tableId' ] . '`
					WHERE 
						' . $subject[ 'partition_field' ] . ' = \'' . $date. '\'
					AND 
						' . $singleIdQuery . '
				';

				if ( empty( $formatDateOnFileName ) === false ) {
					$dateFormat = date( $formatDateOnFileName, strtotime( $date ) ) . '_';
				}

				$year = date( 'Y', strtotime( $date ) );
				$month = date( 'm', strtotime( $date ) );
				$day = date( 'd', strtotime( $date ) );

				$idValue = empty( $idTransformationCallback ) === false ? $idTransformationCallback( $id ) : $id;

				$des[ 'projectId' ] = $BucketDestination[ 'projectId' ];
				$des[ 'bucketName' ] = $BucketDestination[ 'bucketName' ];
				$des[ 'objectName' ] = $__objectPrefix . $year . '/' . $month . '/' . $day . '/' . $objectFileName . '_' . $dateFormat . $idValue . '.csv';

				$this->ExportFromQuery( $query, null, $des, false, 'csv', true, false );
			}
		}
	}

	/**
	 * return active id for a table base on a date
	 *
	 * @param	array	$bigQueryTable			table to be query
	 * @param	string	$idFieldName			id field name to search
	 * @param	string	$partitionDateFieldName	partition field name
	 * @param	string	$date
	 *
	 * @throws	\Google\Cloud\Core\Exception\GoogleException
	 * @throws	\Google\Cloud\Core\Exception\ServiceException
	 *
	 * @return array
	 */

	public function getActiveIdsForDate( array $bigQueryTable, string $idFieldName, string $partitionDateFieldName, string $date ): array {
		$idQuery = /** @lang bq */ '
				SELECT
					' . $idFieldName . '
				FROM
					`' . $bigQueryTable[ 'projectId' ] . '.' . $bigQueryTable[ 'datasetId' ] . '.' . $bigQueryTable[ 'tableId' ] . '`
				WHERE 
					' . $partitionDateFieldName. ' = \'' . $date . '\'
				GROUP BY 1
			';

		return ArrayHelper::array_flatten( $this->runQuery( $idQuery, false ) );
	}

	/**
	 * return active id for a table base on a date
	 *
	 * @param	array		$bigQueryTable			table to be query
	 * @param	string		$partitionDateFieldName	partition field name
	 * @param 	mixed 		$id
	 * @param	string|null	$idFieldName
	 * @param	string|null	$idFieldType
	 *
	 * @return	array
	 *
	 * @throws	\Exception
	 */

	public function getTableDataLifeTime( array $bigQueryTable, string $partitionDateFieldName, $id = null, ?string $idFieldName = null, $idFieldType = null ): array {
		$whereStatement = '';

		if ( empty( $id ) === false ) {

			if( empty( $idFieldName ) === false && empty( $idFieldType ) === false ) {

				if ( $idFieldType === 'STRING' ) {
					$singleIdQuery = $idFieldName . ' = \'' . $id . '\'';
				}
				else {
					$singleIdQuery = $idFieldName . ' = ' . $id ;
				}

				$whereStatement = 'WHERE ' . $singleIdQuery;
			}
			else {
				throw new \Exception( 'idFieldType or $idFieldName missing' );
			}
		}

		$datesRangeQuery = /** @lang bq */ '
			SELECT
				CAST(MIN(' . $partitionDateFieldName . ') AS string) min,
				CAST(MAX(' . $partitionDateFieldName . ') AS string) max
			FROM
				`' . $bigQueryTable[ 'projectId' ] . '.' . $bigQueryTable[ 'datasetId' ] . '.' . $bigQueryTable[ 'tableId' ] . '`
				' . $whereStatement . '
		';

		$datesRange = $this->runQuery( $datesRangeQuery, false );

		$start_date = date( 'Y-m-d', strtotime( $datesRange[ 0 ][ 'min' ] ) );
		$end_date = date( 'Y-m-d', strtotime( $datesRange[ 0 ][ 'max' ] ) );

		return array( $start_date, $end_date );
	}

	/**
	 * backup a table with in a different dataset
	 *
	 * @param array $bigQueryTable
	 * @param string $suffix
	 * @param bool $deleteOriginalTable
	 *
	 * @return bool
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 */

	public function backupTable( array $bigQueryTable, string $suffix, bool $deleteOriginalTable = false ): bool {

		$bigQueryClient = $this->getBigQueryClient( );

		if ( $bigQueryClient->dataset( $bigQueryTable[ 'datasetId' ] . '_' . $suffix )->exists( ) === false ) {
			$bigQueryClient->createDataset( $bigQueryTable[ 'datasetId' ] . '_' . $suffix );
		}

		$des[ 'projectId' ] = $bigQueryTable[ 'projectId' ];
		$des[ 'datasetId' ] = $bigQueryTable[ 'datasetId' ] . '_' .$suffix;
		$des[ 'tableId' ] = $bigQueryTable[ 'tableId' ];

		$this->copyTable( $bigQueryTable, $des );

		if ( $deleteOriginalTable === true ) {
			$this->deleteTable( $bigQueryTable );
		}

		return true;
	}

	/**
	 * backup a dataset
	 *
	 * @param array $bigQueryTable
	 * @param string $suffix
	 * @param bool $deleteOriginalDataset
	 *
	 * @return bool
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 */

	public function backupDataset( array $bigQueryTable, string $suffix, $deleteOriginalDataset = false ): bool {
		$bigQueryClient = $this->getBigQueryClient( );

		if ( $bigQueryClient->dataset( $bigQueryTable[ 'datasetId' ] )->exists( ) === true ) {
			foreach ( $this->listDatasetTables( $bigQueryTable ) as $table ) {
				$des[ 'projectId' ] = $bigQueryTable[ 'projectId' ];
				$des[ 'datasetId' ] = $bigQueryTable[ 'datasetId' ];
				$des[ 'tableId' ] = $table;

				$this->backupTable( $des, $suffix, $deleteOriginalDataset );
			}

			if ( $deleteOriginalDataset === true ) {
				$bigQueryClient->dataset( $bigQueryTable[ 'datasetId' ] )->delete( );
			}

			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * get missing dates from a table, if and id is given will check missing dates for that particular id
	 *
	 * @param array $subject
	 * @param null $id
	 *
	 * @return array
	 *
	 * @throws \Google\Cloud\Core\Exception\GoogleException
	 * @throws \Google\Cloud\Core\Exception\ServiceException
	 * @throws \Exception
	 */

	public function getMissingDates( array $subject, $id = null ): array {
		list( $star_date, $end_date ) = $this->getTableDataLifeTime( $subject[ 'table' ], $subject[ 'partition_field' ], $id , $subject[ 'id' ], $subject[ 'id_type' ] );

		$allDates = DateHelper::getDatesByRange( $star_date, $end_date );

		$whereStatement = '';

		if ( empty( $id ) === false ) {
			if ( $subject [ 'id_type' ] === 'STRING' ) {
				$singleIdQuery = $subject[ 'id' ] . ' = \'' . $id . '\'';
			}
			else {
				$singleIdQuery = $subject[ 'id' ] . ' = ' . $id ;
			}

			$whereStatement = 'WHERE ' . $singleIdQuery;
		}

		$query = '
		SELECT  
			CAST(CAST( ' . $subject[ 'partition_field' ] . ' as DATE) as STRING) as date
		FROM
			`' . $subject[ 'table' ][ 'projectId' ] . '.' . $subject[ 'table' ][ 'datasetId' ] . '.' . $subject[ 'table' ][ 'tableId' ] . '`
			' . $whereStatement .'
		GROUP BY 1
		ORDER BY 1 DESC;
		';

		$tableExistingDates = ArrayHelper::array_flatten( $this->runQuery( $query, false ) );

		$missingDates = array_diff( $allDates, $tableExistingDates );

		return $missingDates;
	}

	/**
	 * @return BigQueryClient
	 */

	public function getBigQueryClient(): BigQueryClient {
		return $this->bigQueryClient;
	}
}